## gitLab
- git remote add main https://gitlab.com/andre.s.gois3/application-node-knex-mysql.git  
- git push -u main master 

## Cria as migrations
- knex migrate:make show_tables

## Cria as tabelas
- knex migrate:up

## Cria a seed | insert da tabela
- knex seed:make users

## Inseri os dados no banco, dados criando nos seeds
- knex seed:run

## Deleta chaves estrangeiras
- SET foreign_key_checks = 0;


############################################
## git
Git global setup
git config --global user.name "André gois"
git config --global user.email "andre.s.gois3@gmail.com"

Create a new repository
git clone https://gitlab.com/andre.s.gois3/application-node-knex-mysql.git
cd application-node-knex-mysql
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/andre.s.gois3/application-node-knex-mysql.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/andre.s.gois3/application-node-knex-mysql.git
git push -u origin --all
git push -u origin --tags
############################################


# git
## Site Oficial
- | [Site git](https://github.com/)

### Configuração de nome e email
- git config --global user.name "Andre"
- git config --global user.email "andre.s.gois3@gmail.com"
- git config --global core.editor "C:\Users\Andre Gois\AppData\Local\Programs\Microsoft VS Code\Code.exe"
### Visualizar informações
- git config user.name
- git config user.email
- git config core.editor
### Criando repositório local
- git init
- git add . | git add nome_do_arquivo
- git commit -m "mensagem que representa as alterações feitas no projeto" | git commit -am "mensagem que representa as alterações feitas no projeto"
### Logs do projeto
- git log
- git log --oneline
- git log --graph
- git log --oneline --graph
### Mostra a branch atual
- git branch
### Visualizar mudanças antes do add
- git diff
### Desfaz alterações antes do add
- git restore nome_arquivo
### Rastrear mudanças
- git checkout fa3329e
### Desfazer alterações depois do add
- git restore --staged README.md
### Recuperar versões anteriores do projeto
- git reset --hard 15cc4fa
### Ramificações no Projeto
- git branch
- git checkout -b teste
### mostra o grafico de todas as branchs
- git log --oneline --graph --all
- | para sair da tela digite "q"
### junta as duas branchs
- git merge nome_branch
- | caso aconteça muitas mudanças eles te dão 3 opções
    - manter a da master
    - manter a da outra branch
    - master ambas
    - analise mais detalhada
### 