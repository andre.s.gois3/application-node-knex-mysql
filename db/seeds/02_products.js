
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('products').del()
    .then(function () {
      // Inserts seed entries
      return knex('products').insert([
        {
          id_product: 1,
          price: 2.90,
          name: 'Açucar'
        },
        {
          id_product: 2,
          price: 3.99,
          name: 'Arroz'
        },
      ]);
    });
};
